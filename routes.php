<?php 
include_once 'core/Config.php';
include_once 'core/Router.php';
include_once 'core/Controller.php';
?>
<!DOCTYPE html>
<html lang="es-MX">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-language" content="es">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url()?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url()?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url()?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url()?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url()?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url()?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url()?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url()?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url()?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url()?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url()?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url()?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url()?>assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url()?>assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title>Registro de pagos</title>

    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css">

    <script>
      const base_url = "<?php echo base_url() ?>";
      const route_name = "<?php echo $_GET['ruta'] ?>";
    </script>
</head>

    <body>
        <div class="container d-flex justify-content-between mt-5">
            <h4 id="route_title"></h4>
            <button onclick="MonthPaymentUrl()" class="btn btn-secondary btn-sm pl-5 pr-5 w-25" >Servicio mensual</button>
        </div>
        <div class="justify-content-center mt-2 mx-5">
            <table id="table_route" class="table table-borderless table-responsive table-sm table-striped table-condensed">
                <thead class="thead-dark">
                    <tr>
                        <th></th>
                        <th class="w-5">Unidad</th>
                        <th class="w-20">Nombre</th>
                        <th class="w-10">Anticipo </th>
                        <th class="w-10">Pago 1</th>
                        <th class="w-10">Pago 2</th>
                        <th class="w-5">Estado</th>
                        <th class="w-10">Fecha de instalación</th>
                        <th class="w-30">Notas</th>
                    </tr>
                </thead>
                <tbody id="tbody_route" ></tbody>
            </table>
        </div>

        <div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="paymentModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title w-100 d-flex justify-content-between" id="paymentModalLabel"></div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="register_payment">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="ticket" class="col-form-label"><span class="text-danger">*</span>Ticket:</label>
                            <div class="custom-file">
                                <input type="file" class="form-control-file" id="ticket" name="file[]" onchange="checkFile()" accept="image/jpeg, image/png" required lang="es">
                            </div>
                        </div>
                        <div class="form-group"  id="amount_form">
                            <label for="amount" class="col-form-label"><span class="text-danger">*</span>Monto del pago:</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">$</div>
                                </div>
                                <input type="number" onkeyup="amountChenged('amount')" min="0" step="0.01" class="form-control" id="amount" required>
                            </div>
                        </div>
                        <div class="form-group"  id="comments_form">
                            <label for="comments" class="col-form-label">Comentarios:</label>
                            <div class="input-group mb-2">
                                <input type="text" class="form-control" id="comments">
                            </div>
                        </div>
                        <div class="card d-none" id="units_card">
                            <div class="card-body bg-light p-2" id="units_card_rows">
                            </div>
                        </div>
                        <div class="card d-none" id="observations_card">
                            <div class="card-body bg-light p-2" id="observations">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Registrar</button>
                    </div>
                    <div class="px-3 pb-3" id="records_section">
                        <div id="records" class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>Registros: </strong> 0
                        </div>
                    </div>
                </form>
            </div>
        </div>
        </div>
    </body>
    <footer>
        <script src="<?php echo base_url() ?>assets/js/jquery-3.3.1.js"></script>
        <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>controllers/payment/script_payment.js"></script>
        <script>
            $(function() {
                getRoutePayments();
            });
        </script>
    </footer>
</html>