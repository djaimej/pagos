<?php 
require_once  '../../models/admin/admin.php';
header("Access-Control-Allow-Origin: *");

class admin{
    private $admin;

	public function __construct(){
        $this->admin = new queriesAdmin();
    }
    public function getRoutePaymentsAdmin(){
        $data = $this->admin->getRoutePaymentsAdmin($_POST["route_name"]);
        echo json_encode($data);
    }
    public function getFile(){
        $data = $this->admin->getFile($_POST["route_payment_id"],$_POST["payment_number"]);
        echo json_encode($data);
    }
    public function getCountFile(){
        $data = $this->admin->getCountFile($_POST["payment_number"],$_POST["route_name"]);
        echo json_encode($data);
    }
    public function updateObservations(){
        $data = $this->admin->updateObservations($_POST["route_payment_id"], $_POST["payment_number"],$_POST["observations"]);
        echo $data;
    }
    public function setStatus(){
        $data = $this->admin->setStatus($_POST["payment_id"],$_POST['status']);
        echo json_encode($data);
    }
    public function getTotalPayment(){
        $data = $this->admin->getTotalPayment($_POST["route_name"]);
        echo json_encode($data);
    }
    public function UpdateUnitStatus(){
        $data = $this->admin->UpdateUnitStatus($_POST["route_payment_id"]);
        echo $data;
    }
    public function UpdateUnitNotes(){
        $data = $this->admin->UpdateUnitNotes($_POST["route_payment_id"],$_POST["notes"]);
        echo $data;
    }
    public function getRoutes(){
        $data = $this->admin->getRoutes();
        echo json_encode($data);
    }
    public function updateInstDate(){
        $data = $this->admin->updateInstDate($_POST["route_payment_id"],$_POST["date_inst"]);
        echo json_encode($data);
    }
    public function deleteFile(){
        $data = $this->admin->deleteFile($_POST["payment_id"]);
        echo json_encode($data);
    }
    public function updateMonthObservations(){
        $data = $this->admin->updateMonthObservations($_POST["payment_ids"],$_POST["month"],$_POST["observations"]);
        echo json_encode($data);
    }
}

$obj = new admin();
if (isset($_POST["action"])){
    if ($_POST["action"]==1) {
        $obj->getRoutePaymentsAdmin();
    }elseif ($_POST["action"]==2) {
        $obj->getFile();
    }elseif ($_POST["action"]==3) {
        $obj->getCountFile();
    }elseif ($_POST["action"]==4) {
        $obj->updateObservations();
    }elseif ($_POST["action"]==5) {
        $obj->setStatus();
    }elseif ($_POST["action"]==6) {
        $obj->getTotalPayment();
    }elseif ($_POST["action"]==7) {
        $obj->UpdateUnitStatus();
    }elseif ($_POST["action"]==8) {
        $obj->UpdateUnitNotes();
    }elseif ($_POST["action"]==9) {
        $obj->getRoutes();
    }elseif ($_POST["action"]==10) {
        $obj->updateInstDate();
    }elseif ($_POST["action"]==11) {
        $obj->deleteFile();
    }elseif ($_POST["action"]==12) {
        $obj->updateMonthObservations();
    }
}
?>