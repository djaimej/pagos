var payment_number, route_payment_id, month, unit;
var typingTimer;
var doneTypingInterval = 3000;  
var payment_ids = [];

const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'MXN',
    minimumFractionDigits: 2
});

async function getRoutePaymentsAdmin() {
    console.log(route_name);
    
    var total1, total2, total3;
    await $.ajax({
        url: `${base_url}controllers/admin/admin_controller.php`,
        cache: false,
        type: 'POST',
        dataType: 'JSON',
        data: { action: 6, route_name: route_name},
        success: function(result){
            total1 = result[0].total != null ? formatter.format(result[0].total) : 'MX$0.00';
            total2 = result[1].total != null ? formatter.format(result[1].total) : 'MX$0.00';
            total3 = result[2].total != null ? formatter.format(result[2].total) : 'MX$0.00';
        },
        error: function(error){
            console.error(error);
        }
    });
    
    var table = "";
    await $.ajax({
        url: base_url + "controllers/admin/admin_controller.php",
        type: "POST",
        dataType: "JSON",
        data: { action: 1, route_name: route_name },
        cache: false,
        success: function(response) {  
            $.each(response, function(index, val) {
                const status = val.status == 't';
                const notes = val.notes != null ? val.notes : '';
                table +=`<tr>
                            <td class="w-5 bg-dark text-white">${val.unit}</td>
                            <td class="w-20">${val.last_name}</td>
                            <td class="w-10"> <button id="buttonP1${val.route_payment_id}" onclick="getFile(1,${val.route_payment_id},'${val.unit}');" class="btn btn-light w-100">Revisar</button></td>
                            <td class="w-10"> <button id="buttonP2${val.route_payment_id}" onclick="getFile(2,${val.route_payment_id},'${val.unit}');" class="btn btn-light w-100">Revisar</button></td>
                            <td class="w-10"> <button id="buttonP3${val.route_payment_id}" onclick="getFile(3,${val.route_payment_id},'${val.unit}');" class="btn btn-light w-100">Revisar</button></td>
                            <td class="w-10"> <button status="${status}" id="st_${val.route_payment_id}" onclick="UpdateUnitStatus(${val.route_payment_id})" class="btn ${status ? 'btn-success' : 'btn-danger'} w-100">${status ? 'Instalada': 'No instalada'}</button></td>
                            <td class="w-10"> <input value="${val.installation_date}" ${!status ? 'disabled' : ''} onchange="updateInstDate(${val.route_payment_id})" type="date" id="inst_${val.route_payment_id}"></td>
                            <td class="w-25"><input onkeydown="notesKeyDown()" onkeyup="notesKeyUp(${val.route_payment_id})" class="form-control text-reset form-control-sm" id="notes_${val.route_payment_id}" value="${notes}" type="text"></td>
                        </tr>`;
            });
            table += `<tr>
                        <td class="w-5 font-weight-bold bg-dark text-white ">Total: </td>
                        <td class="w-20">...</td>
                        <td class="w-10 text-center">${total1}</td>
                        <td class="w-10 text-center">${total2}</td>
                        <td class="w-10 text-center">${total3}</td>
                        <td class="w-10 text-center"></td>
                        <td class="w-10 text-center"></td>
                        <td class="w-25 text-center"></td>
                    </tr>`;
        },error: function (err) {
            console.error(err);
        }
    });
    
    table = table != "" ? table : "<tr><td class='alert-warning'></td><td colspan='5' class='w-100 alert-warning' role='alert'>No hay datos que mostrar</td></tr>";
    
    $("#tbody_route").html(table);
    $("#route_title").html(`Registros ${route_name}`);
}

async function getMonthPayments() {
    await $.ajax({
        url: `${base_url}controllers/payment/payment_controller.php`,
        type: "POST",
        dataType: "JSON",
        data: { action: 7, route_name: route_name },
        cache: false,
        success: function(response) { 
            var table = '';
            $.each(response, function(index, val) {
                var months = ["ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC"];
                var d = new Date(val.month);
                const color = val.status == null ? 'info' : response[0].status == 1 ? 'success' : response[0].status == 2 ? 'danger' : 'warning';
                const label = val.status == null ? 'en revisiòn' : response[0].status == 1 ? 'vàlido' : response[0].status == 2 ? 'no vàlido' : 'informaciòn requerida';
                table +=`<tr>
                            <td class="bg-dark"></td>
                            <td class="w-10 bg-dark text-white">${index+1}</td>
                            <td class="w-20">${val.date}</td>
                            <td class="w-10">${months[d.getMonth()]} ${d.getFullYear()}</td>
                            <td class="w-40">${val.comment != null ? val.comment : ''}</td>
                            <td class="w-10"> <span style="height: 1.5em;" class="badge badge-pill badge-${color}">${label}</span></td>
                            <td class="w-10"><button id="buttonP3" onclick="viewDetails('${val.month}');" class="btn btn-sm btn-secondary w-100">Ver</button></td>
                        </tr>`;
            });
            table = table != "" ? table : "<tr><td class='alert-warning'></td><td colspan='6' class='w-100 alert-warning' role='alert'>No hay datos que mostrar</td></tr>";
            $("#tbody_month_route").html(table);
            $("#route_title").html(`Pagos mensuales de ${route_name}`);
        },error: function (err) {
            console.error(err);
        }
    });
}

async function getFile(payment_number,route_payment_id,unit) {
    this.payment_number = payment_number;
    this.route_payment_id = route_payment_id;
    this.unit = unit;
    payment_ids = [];
    await $.ajax({
        url: `${base_url}controllers/admin/admin_controller.php`,
        cache: false,
        type: 'POST',
        dataType: 'JSON',
        data: { action: 2, route_payment_id:route_payment_id, payment_number: payment_number},
        success: function(response){
            var body = "";
            $.each(response, function(index, registry) {
                body += 
                    `<tr>
                        <td></td>
                        <td class="w-20">${formatter.format(+`${registry.monto}`)}</td>
                        <td class="w-20">${registry.date}</td>
                        <td class="w-20">
                            <a class="badge badge-secondary"  target="_blank" href="${base_url}storage/${route_name}/${unit}/${registry.file}">
                                ${unit} Archivo ${(index+1)}
                                <img class="ticket-img" src="${base_url}storage/${route_name}/${unit}/${registry.file}" onerror='this.onerror = null; this.src="${base_url}storage/${route_name}/U/${registry.file}"' alt="">
                            </a>
                        </td>
                        <td class="w-35 text-break">
                            ${(registry.comment != null ? registry.comment : '')}
                        </td>
                        <td class="w-5">
                        <button type="button" class="btn badge badge-pill badge-danger" data-toggle="modal" data-target="#modalDelete" data-route_p_id="${registry.payment_id}">X</button> 
                        </td>
                    </tr>`;
                (index == 0 ) ? $("#observations").val(registry.observations) : null;
                const payment_label = payment_number == 1 ? 'Anticipo' : payment_number == 2 ? 'Pago 1' : 'Pago 2';
                $("#modalTitle").html(`${payment_label} - ${unit}`);
                payment_ids.push(registry.payment_id);
                let label = registry.status==1 ? 'válido' : registry.status==2 ? 'incorrecto' : registry.status==3 ? 'info. req.': 'pendiente';
                let color = registry.status==1 ? 'success' : registry.status==2 ? 'danger' : registry.status==3 ? 'warning' : 'info';
                $("#bStatus").html(label);
                $("#bStatus").removeClass();
                $("#bStatus").addClass(`badge badge-pill badge-${color}`);
            });
            $("#tbody-file").html(body);
        },
        error: function(error){
            console.error(error);
        }
    });
    $("#filesModal").modal("show");
}

$('#modalDelete').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var id = button.data('route_p_id');
    var modal = $(this);
    modal.find('.modal-body input').val(id);
});

async function deleteFile(){
    var res = 0;
    await $.ajax({
        url: base_url + "controllers/admin/admin_controller.php",
        type: 'POST',
        dataType: 'JSON',
        data: { action: 11, payment_id: $('#payment_id').val() },
        cache: false,
        success: function(response) {
            res = response;
            if (response == 1) {
                $('#modalDelete').modal('hide');
            }
        },error: function (err) {
            console.error(err);
        }
    });
    if(res == 1){
        await getFile(payment_number,route_payment_id,unit);
        for(var i=1;i<4;i++){
            getCountFile(i);
        }
        alert("Registro eliminado con éxito");
    } else{
        alert('Error al borrar registro');
    }
}

async function deleteMonthPayment(){
    var res = 0;
    await new Promise(resolve =>{
        payment_ids.forEach(async payment_id => {
            const last = payment_ids.length -1;
            await $.ajax({
                url: base_url + "controllers/admin/admin_controller.php",
                type: 'POST',
                dataType: 'JSON',
                data: { action: 11, payment_id: payment_id },
                cache: false,
                success: function(response) {
                    res = response;
                    if(payment_ids[last] == payment_id){
                        resolve();
                    }
                },error: function(err) {
                    console.error(err);
                }
            });
        });
    })
    if(res == 1){ 
        $('#modalDelete').modal('hide');
        alert("Registro eliminado con éxito");
        location.reload();
    } else{
        alert('Error al borrar registro');
    }
    
}


function getCountFile(payment_number){
    $.ajax({
        url: `${base_url}controllers/admin/admin_controller.php`,
        cache: false,
        type: 'POST',
        dataType: 'JSON',
        data: { action: 3, payment_number: payment_number, route_name: route_name},
        success: function(response){
            $.each(response, function(index, val) {
                $("#buttonP"+payment_number+val.route_payment_id).html("Registros ("+val.payments+")");
                $('#buttonP'+payment_number+val.route_payment_id).removeClass();
                $('#buttonP'+payment_number+val.route_payment_id).addClass("btn btn-primary  w-100");
                
                if(val.status==1){
                    $('#buttonP'+payment_number+val.route_payment_id).removeClass();
                    $('#buttonP'+payment_number+val.route_payment_id).addClass("btn btn-success w-100");
                }
                if(val.status==2){
                    $('#buttonP'+payment_number+val.route_payment_id).removeClass();
                    $('#buttonP'+payment_number+val.route_payment_id).addClass("btn btn-danger w-100");
                }
                if(val.status==3){
                    $('#buttonP'+payment_number+val.route_payment_id).removeClass();
                    $('#buttonP'+payment_number+val.route_payment_id).addClass("btn btn-warning w-100");
                }
            });
        },
        error: function(error){
            console.error(error);
        }
    });
}

async function updateObservations() {
    const observations = $('#observations').val().length > 0 ? $('#observations').val(): null;
    await $.ajax({
        url: `${base_url}controllers/admin/admin_controller.php`,
        cache: false,
        type: 'POST',
        dataType: 'JSON',
        data: { action: 4, route_payment_id: route_payment_id, payment_number: payment_number, observations: observations},
        success: function(result){
            console.log(result);
        },
        error: function(error){
            console.error(error);
        }
    });
}

async function updateMonthObservations() {
    const observations = $('#observations').val().length > 0 ? $('#observations').val(): null;
    
    await $.ajax({
        url: `${base_url}controllers/admin/admin_controller.php`,
        cache: false,
        type: 'POST',
        dataType: 'JSON',
        data: { action: 12, payment_ids: JSON.stringify(payment_ids), month: month, observations: observations},
        success: function(result){
            console.log(result);
        },
        error: function(error){
            console.error(error);
        }
    });
}

async function updateInstDate(id) {
    const date_inst = $(`#inst_${id}`).val().length == 0 ? 'null' : `'${$(`#inst_${id}`).val()}'`;
    
    await $.ajax({
        url: `${base_url}controllers/admin/admin_controller.php`,
        cache: false,
        type: 'POST',
        dataType: 'JSON',
        data: { action: 10, route_payment_id: id, date_inst: date_inst},
        success: function(result){
            console.log(result);
        },
        error: function(error){
            console.error(error);
        }
    });
}

async function setStatus(status){
    payment_ids.forEach(async payment_id => {
        await $.ajax({
            url: `${base_url}controllers/admin/admin_controller.php`,
            cache: false,
            type: 'POST',
            dataType: 'JSON',
            data: { action: 5, payment_id: payment_id, status: status},
            success: function(result){
                let label = status==1 ? 'válido' : status==2 ? 'incorrecto' : 'info. req.';
                let color = status==1 ? 'success' : status==2 ? 'danger' : 'warning';
                $("#bStatus").html(label);
                $("#bStatus").removeClass();
                $("#bStatus").addClass(`badge badge-pill badge-${color}`);
            },
            error: function(error){
                console.error(error);
            }
        });
    });
}

function viewDetails(month) {
    this.month = month;
    payment_ids = [];
    $.ajax({
        url: `${base_url}controllers/payment/payment_controller.php`,
        cache: false,
        type: 'POST',
        dataType: 'JSON',
        data: { action: 8, route_name: route_name, month: month},
        success: function(response){
            $('#ticket_img').html(`<img class="w-100" src="${base_url}storage/${route_name}/M/${response[0].file}" alt="">`);
            var rows = '';
            var montoTotal = 0;
            $.each(response, function(i, unit) {
                rows += i % 5 == 0 ? '<div class="row">' : '';
                rows += `<div class="col">
                            <span>${unit.unit} </span>
                        </div>`;
                rows += i % 5 == 4 ? '</div>' : '';
                montoTotal += +`${unit.monto}`;
                payment_ids.push(+`${unit.payment_id}`);
            });
            let label = response[0].status==1 ? 'válido' : response[0].status==2 ? 'incorrecto' : response[0].status==3 ? 'info. req.' : 'pendiente';
            let color = response[0].status==1 ? 'success' : response[0].status==2 ? 'danger' : response[0].status==3 ? 'warning' : 'info';
            $("#bStatus").html(label);
            $("#bStatus").removeClass();
            $("#bStatus").addClass(`badge badge-pill badge-${color}`);
            $('#units_rows').html(rows);
            $('#total_amount').html(formatter.format(+`${montoTotal}`));
            $('#total_by_unit').html(formatter.format(+`${response[0].monto}`));
            $('#observations').val(response[0].observations);
            console.log(payment_ids);
            
        },
        error: function(error){
            console.error(error);
        }
    });
    $("#modalTitle").html(`Mensulidad - ${route_name}`);
    $("#detailsModal").modal("show");
}

$('#filesModal').on('hidden.bs.modal', async function () {
    await updateObservations();
    getCountFile(payment_number);
    $("#observations").val(null);
});

$('#detailsModal').on('hidden.bs.modal', async function () {
    await updateMonthObservations();
    $("#observations").val(null);
    getMonthPayments();
});

function UpdateUnitStatus(route_payment_id) {
    $.ajax({
        url: `${base_url}controllers/admin/admin_controller.php`,
        cache: false,
        type: 'POST',
        dataType: 'JSON',
        data: { action: 7, route_payment_id:route_payment_id},
        success: function (result) {
            if (result == 1) {
                const status = $(`#st_${route_payment_id}`).attr("status") == 'true';
                
                $(`#inst_${route_payment_id}`).attr("disabled", status);
                
                $(`#st_${route_payment_id}`).attr("status", !status);
                $(`#st_${route_payment_id}`).html(!status ? 'Instalada' : 'No instalada');
                $(`#st_${route_payment_id}`).removeClass(!status ? 'btn-danger' : 'btn-success');
                $(`#st_${route_payment_id}`).addClass(status ? 'btn-danger' : 'btn-success');
            } else {
                
            }
            
        },
        error: function(result){
            console.log(result);
        }
    });
}

function notesKeyUp(route_payment_id) {
    $(`#notes_${route_payment_id}`).addClass('border border-info');
    localStorage.setItem("route_payment_id", route_payment_id)
    clearTimeout(typingTimer);
    typingTimer = setTimeout(UpdateUnitNotes, doneTypingInterval);
}

function notesKeyDown() {
    clearTimeout(typingTimer);
}

function UpdateUnitNotes() {
    const route_payment_id = localStorage.getItem("route_payment_id");
    const notes = $(`#notes_${route_payment_id}`).val();
    $.ajax({
        url: `${base_url}controllers/admin/admin_controller.php`,
        cache: false,
        type: 'POST',
        dataType: 'JSON',
        data: { action: 8, route_payment_id:route_payment_id, notes: notes},
        success: function (result) {
            if (result == 1) {
                $(`#notes_${route_payment_id}`).removeClass('border border-info');
                $(`#notes_${route_payment_id}`).addClass('border border-success');
            } else {
                $(`#notes_${route_payment_id}`).removeClass('border border-info');
                $(`#notes_${route_payment_id}`).addClass('border border-danger');
            }
            
        },
        error: function(result){
            console.log(result);
        }
    });
}


function loadRoutesAdm(){
    $.ajax({
        url: `${base_url}controllers/admin/admin_controller.php`,
        cache: false,
        type: 'POST',
        dataType: 'JSON',
        data: { action: 9},
        success: function (result) {
          let content = "";
            $.each(result, function(index, val){
                let units = val.units;
                let ant = val.ant;
                let p1 = val.p1;
                let p2 = val.p2;
                let mens = val.mens;
              content += `<tr scope="row" >
                            <td></td>
                            <td class="w-30">${val.route}</td>
                            <td class="w-15 font-weight-bold ${ant[0] == units ? (ant[0] == ant[1] ? 'text-success' : 'text-warning'): (ant[0] > 0 ? 'text-warning' : '')} ">${ant}/${units}</td>
                            <td class="w-15 font-weight-bold ${p1[0] == units ? (p1[0] == p1[1] ? 'text-success' : 'text-warning'): (p1[0] > 0 ? 'text-warning' : '')}">${p1}/${units}</td>
                            <td class="w-15 font-weight-bold ${p2[0] == units ? (p2[0] == p2[1] ? 'text-success' : 'text-warning'): (p2[0] > 0 ? 'text-warning' : '')}">${p2}/${units}</td>
                            <td class="w-15 font-weight-bold ${mens[0] == units ? (mens[0] == mens[1] ? 'text-success' : 'text-warning'): (mens[0] > 0 ? 'text-warning' : '')}">${mens}/${units}</td>
                            <td class="w-10">
                                <button class="btn btn-info btn-sm w-100" onclick="routeDataAdm('${val.route}')">Acceder</button>
                            </td>
                          </tr>`;
            });
            $("#content-table-route").append(content);
        },
        error: function(result){
            console.log(result);
        }
    });
}

function routeDataAdm(route){
  window.location.href = `${base_url}admin/routes.php?ruta=${route}`;
}

function routeData(){
  window.location.href = `${base_url}admin/routes.php`;
}

function MonthPaymentUrl(){
    window.location.href = `${base_url}admin/month.php?ruta=${route_name}`;
}