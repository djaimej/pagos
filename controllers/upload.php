<?php
date_default_timezone_set("America/Mexico_City");

$route_name = $_POST["route_name"];
$unit = $_POST["unit"];
$payment_number = $_POST["payment_number"];
$current_date = date('Y-m-d H.i.s');


$path = $_FILES['file']['name'];
$ext = pathinfo($path, PATHINFO_EXTENSION);

if(!file_exists('../storage/'.$route_name.'/'.$unit.'/')){
    mkdir('../storage/'.$route_name.'/'.$unit.'/',0777,true);
}

$target_path = '../storage/'.$route_name.'/'.$unit.'/';

$filename = $route_name.'-'.$unit.'-P'.$payment_number.' '.$current_date.'.'.$ext;

$target_path = $target_path.basename($filename);
if(move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) { 
    echo $filename;
    // return "success";
}else{
    echo "error";
} 

?>