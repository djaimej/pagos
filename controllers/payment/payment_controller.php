<?php 
require_once  '../../models/payment/payment.php';
header("Access-Control-Allow-Origin: *");

class payment{
    private $payment;

	public function __construct(){
        $this->payment = new queriesPayment();
    }
    public function getRoutePayments(){
        $data = $this->payment->getRoutePayments($_POST["route_name"]);
        echo json_encode($data);
    }
    public function addPayment(){
        $data = $this->payment->addPayment($_POST['route_payment_id'],$_POST['monto'],$_POST['payment_number'],$_POST['file'],$_POST['comment']);
        echo json_encode($data);
    }
    public function getPaymentData(){
        $data = $this->payment->getPaymentData($_POST['route_payment_id'],$_POST['payment_number']);
        echo json_encode($data);
    }
    public function getRoutes(){
        $data = $this->payment->getRoutes();
        echo json_encode($data);
    }
    public function addMonthlyPayment(){
        $data = $this->payment->addMonthlyPayment($_POST['route_units'],$_POST['monto'],$_POST['file'],$_POST['comment'],$_POST['month']);
        echo $data;
    }
    public function addPaymentMultiple(){
        $data = $this->payment->addPaymentMultiple($_POST['route_units'],$_POST['monto'],$_POST['payment_number'],$_POST['file'],$_POST['comment']);
        echo json_encode($data);
    }
    public function getMonthPayments(){
        $data = $this->payment->getMonthPayments($_POST['route_name']);
        echo json_encode($data);
    }
    public function viewDetails(){
        $data = $this->payment->viewDetails($_POST['route_name'],$_POST['month']);
        echo json_encode($data);
    }
}

$obj = new payment();
if (isset($_POST["action"])){
    if ($_POST["action"]==1) {
        $obj->getRoutePayments();
    }elseif ($_POST["action"]==2) {
        $obj->addPayment();
    }elseif ($_POST["action"]==3) {
        $obj->getPaymentData();
    }elseif ($_POST["action"]==4) {
        $obj->getRoutes();
    }elseif ($_POST["action"]==5) {
        $obj->addMonthlyPayment();
    }elseif ($_POST["action"]==6) {
        $obj->addPaymentMultiple();
    }elseif ($_POST["action"]==7) {
        $obj->getMonthPayments();
    }elseif ($_POST["action"]==8) {
        $obj->viewDetails();
    }
}
?>