var payment_number, route_payment_id, unit;
$(function() {
    $('#current_year').html(new Date().getFullYear());
});
var paymentArray = new Array;

const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'MXN',
    minimumFractionDigits: 2
});

function amountChenged(idAmount) {
    var valid = /^\d{0,10}(\.\d{0,2})?$/.test( $(`#${idAmount}`).val().replace(".", "") ),
        val = $(`#${idAmount}`).val().replace(".", "");
    if( !valid ) {
        // invalid input, erase character
        $(`#${idAmount}`).val(val.substring(0, val.length-1));
    } else if( val.length > 2 ) {
        // valid input, set decimal point if string is longer than 3 characters
        $(`#${idAmount}`).val(val.substring(0,val.length-2)+"."+val.substring(val.length-2))
    }
}

async function getRoutePayments() {
    var itsRegistered = null;
    var type = null;
    await $.ajax({
        url: `${base_url}controllers/payment/payment_controller.php`,
        type: "POST",
        dataType: "JSON",
        data: { action: 1, route_name: route_name },
        cache: false,
        success: function(response) { 
            var table =`<tr>
                            <td class="bg-dark"></td>
                            <td class="w-5 bg-dark text-white">Multiple</td>
                            <td class="w-20">...</td>
                            <td class="w-10"> <button id="buttonP1" onclick="addMultiplePayment(1);" class="btn btn-secondary w-100">Registrar</button></td>
                            <td class="w-10"> <button id="buttonP2" onclick="addMultiplePayment(2);" class="btn btn-secondary w-100">Registrar</button></td>
                            <td class="w-10"> <button id="buttonP3" onclick="addMultiplePayment(3);" class="btn btn-secondary w-100">Registrar</button></td>
                            <td class="w-5"> </td>
                            <td class="w-10"> </td>
                            <td class="w-30"> </td>
                        </tr>`;
            $.each(response, function(index, val) {
                paymentArray.push([val['route_payment_id']]);
                itsRegistered = val.type != null  && itsRegistered == null ? true : itsRegistered;
                type = val.type != null  && itsRegistered ? val.type : type;
                table +=`<tr>
                            <td class="bg-dark"></td>
                            <td class="w-5 bg-dark text-white">${val.unit}</td>
                            <td class="w-20">${val.last_name}</td>
                            <td class="w-10"> <button id="buttonP1${val.route_payment_id}" onclick="addPayment(1, '${val.unit}', ${val.route_payment_id});" class="btn btn-primary w-100">Registrar</button></td>
                            <td class="w-10"> <button id="buttonP2${val.route_payment_id}" onclick="addPayment(2, '${val.unit}', ${val.route_payment_id});" class="btn btn-primary w-100">Registrar</button></td>
                            <td class="w-10"> <button id="buttonP3${val.route_payment_id}" onclick="addPayment(3, '${val.unit}', ${val.route_payment_id});" class="btn btn-primary w-100">Registrar</button></td>
                            <td class="w-5">${(val.status == 't' ? '<span class="badge badge-pill badge-success">Instalada</span>' : '<span class="badge badge-pill badge-danger">NO instalada</span>')}</td>
                            <td class="w-10">${(val.installation_date != null ? val.installation_date : '')}</td>
                            <td class="w-30">${val.notes != null ? val.notes : ''}</td>
                        </tr>`;
            });
            for(var i=1;i<4;i++){
                getCountFile(i);
            }
            table = table != "" ? table : "<tr><td class='alert-warning'></td><td colspan='5' class='w-100 alert-warning' role='alert'>No hay datos que mostrar</td></tr>";
            $("#tbody_route").html(table);
            $("#route_title").html(`Pagos de ${route_name}`);
        },error: function (err) {
            console.error(err);
        }
    });
}

async function getMonthPayments() {
    await $.ajax({
        url: `${base_url}controllers/payment/payment_controller.php`,
        type: "POST",
        dataType: "JSON",
        data: { action: 7, route_name: route_name },
        cache: false,
        success: function(response) { 
            var table = '';
            $.each(response, function(index, val) {
                var months = ["ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC"];
                var d = new Date(val.month);
                const color = val.status == null ? 'info' : response[0].status == 1 ? 'success' : response[0].status == 2 ? 'danger' : 'warning';
                const label = val.status == null ? 'en revisiòn' : response[0].status == 1 ? 'vàlido' : response[0].status == 2 ? 'no vàlido' : 'informaciòn requerida';
                table +=`<tr>
                            <td class="bg-dark"></td>
                            <td class="w-10 bg-dark text-white">${index+1}</td>
                            <td class="w-20">${val.date}</td>
                            <td class="w-10">${months[d.getMonth()]} ${d.getFullYear()}</td>
                            <td class="w-40">${val.observations != null ? val.observations : ''}</td>
                            <td class="w-10"> <span style="height: 1.5em;" class="badge badge-pill badge-${color}">${label}</span></td>
                            <td class="w-10"><button id="buttonP3" onclick="viewDetails('${val.month}');" class="btn btn-sm btn-secondary w-100">Ver</button></td>
                        </tr>`;
            });
            table = table != "" ? table : "<tr><td class='alert-warning'></td><td colspan='6' class='w-100 alert-warning' role='alert'>No hay datos que mostrar</td></tr>";
            $("#tbody_route").html(table);
            $("#route_title").html(`Pagos mensuales de ${route_name}`);
        },error: function (err) {
            console.error(err);
        }
    });
}

async function addPayment(payment_number, unit, route_payment_id) {
    this.payment_number = payment_number;
    this.route_payment_id = route_payment_id;
    this.unit = `${unit}`;
    var badge = '';
    var color = '';
    var label = '';
    await $.ajax({
        url: `${base_url}controllers/payment/payment_controller.php`,
        cache: false,
        type: "POST",
        dataType: "JSON",
        data: { action: 3, route_payment_id: route_payment_id, payment_number: payment_number},
        success: function(response){
            $('#units_card').addClass('d-none');
            $('#observations_card').addClass('d-none');
            $("#observations").html(null);
            const records = response.length;
            $("#records").html('<strong>Registros: </strong>'+records);
            if(records > 0) {
                if (response[0].observations != null) {
                    $('#observations_card').removeClass('d-none');
                    $("#observations").html(`<strong>Observaciones:</strong> ${response[0].observations}`);
                }
                color = response[0].status == null ? 'info' : response[0].status == 1 ? 'success' : response[0].status == 2 ? 'danger' : 'warning';
                label = response[0].status == null ? 'en revisiòn' : response[0].status == 1 ? 'vàlido' : response[0].status == 2 ? 'no vàlido' : 'informaciòn requerida';
            }else {
                color = 'info';
                label = 'en revisiòn';
            }
            $("#records").removeClass();
            $("#records").addClass(`alert-${color} alert alert-dismissible fade show`);
            badge = records > 0 ? `<span style="height: 1.5em;" class="badge badge-pill badge-${color}">${label}</span>` : '';
            

        },error: function (err) {
            console.error(err);
        }
    });
    const payment_label = payment_number == 1 ? 'Anticipo' : payment_number == 2 ? 'Pago 1' : payment_number == 3 ? 'Pago 2' : 'Mensualidad';
    $("#paymentModalLabel").html(`<h5>${payment_label} - ${unit}</h5> ${badge}`);
    $("#paymentModal").modal("show");
}

async function addMonthlyPayment() {
    this.payment_number = 4;
    this.unit = 'M';
    paymentArray = [];
    await $.ajax({
        url: `${base_url}controllers/payment/payment_controller.php`,
        cache: false,
        type: "POST",
        dataType: "JSON",
        data: { action: 1, route_name: route_name},
        success: function(response){
            var rows = '';
            $.each(response, function(i, unit) {
                paymentArray.push(+unit.route_payment_id);
                rows += i % 4 == 0 ? '<div class="row">' : '';
                rows += `<div class="col">
                            <div class="custom-control custom-checkbox mr-sm-2">
                                <input type="checkbox" value="${unit.route_payment_id}" checked class="custom-control-input" id="cb_${unit.route_payment_id}">
                                <label class="custom-control-label" for="cb_${unit.route_payment_id}"></label>
                                <span>${unit.unit}</span>
                            </div>
                        </div>`;
                rows += i % 4 == 3 ? '</div>' : '';
            });
            $('#units_card_rows').html(rows);
            
        },error: function (err) {
            console.error(err);
        }
    });
    $("#paymentModalMonthLabel").html(`<h5>Mensualidad - ${route_name}</h5>`);
    $("#paymentModalMonth").modal("show");
}

async function addMultiplePayment(payment_number) {
    this.payment_number = payment_number;
    this.unit = 'U';
    paymentArray = [];
    await $.ajax({
        url: `${base_url}controllers/payment/payment_controller.php`,
        cache: false,
        type: "POST",
        dataType: "JSON",
        data: { action: 1, route_name: route_name},
        success: function(response){
            $('#units_card').removeClass('d-none');
            var rows = '';
            $.each(response, function(i, unit) {
                paymentArray.push(+unit.route_payment_id);
                rows += i % 4 == 0 ? '<div class="row">' : '';
                rows += `<div class="col">
                            <div class="custom-control custom-checkbox mr-sm-2">
                                <input type="checkbox" value="${unit.route_payment_id}" checked class="custom-control-input" id="cb_${unit.route_payment_id}">
                                <label class="custom-control-label" for="cb_${unit.route_payment_id}"></label>
                                <span>${unit.unit}</span>
                            </div>
                        </div>`;
                rows += i % 4 == 3 ? '</div>' : '';
            });
            $('#units_card_rows').html(rows);
            
        },error: function (err) {
            console.error(err);
        }
    });
    const payment_label = payment_number == 1 ? 'Anticipo' : payment_number == 2 ? 'Pago 1' : payment_number == 3 ? 'Pago 2' : 'Mensualidad';
    $("#paymentModalLabel").html(`<h5>${payment_label} - ${route_name}</h5>`);
    $("#paymentModal").modal("show");
}

$("#register_payment").submit(function(event) {
    var unites_quantity = [];
    event.preventDefault();
    var form = new FormData();
    form.append("file",$("#ticket")[0].files[0]);
    form.append("unit", unit);
    form.append("route_name", route_name);
    form.append("payment_number", payment_number);

    paymentArray.forEach(element => {
        if($('#cb_' + element).prop('checked') ){
            unites_quantity.push(element);
         }
    });

    $.ajax({
        url: `${base_url}controllers/upload.php`,
        type: "POST",
        data: form,
        processData: false,
        contentType: false,
        success: function(file){
            if(!`${file}`.includes("error")){
                if(unites_quantity.length == 0){
                    $.ajax({
                        url: `${base_url}controllers/payment/payment_controller.php`,
                        cache: false,
                        type: "POST",
                        dataType: "JSON",
                        data: { action: 2, route_payment_id: route_payment_id, monto: $('#amount').val(), payment_number: payment_number, file: `${file}`, comment: $('#comments').val()},
                        success: function(response){
                            console.log(response);
                            if(`${response}` !== 'error'){
                                alert('pago registrado con exito!');
                                $("#paymentModal").modal("hide");
                                location.reload();
                            }
                            
                        },error: function (err) {
                            console.error(err);
                        }
                    });
                }else{
                    const montoTotal =  $('#amount').val();
                    const mont = montoTotal / unites_quantity.length;
                    $.ajax({
                        url: `${base_url}controllers/payment/payment_controller.php`,
                        cache: false,
                        type: "POST",
                        dataType: "JSON",
                        data: { action: 6, route_units: JSON.stringify(unites_quantity), monto: mont, payment_number: payment_number, file: `${file}`, comment: $('#comments').val()},
                        success: function(response){
                            console.log(response);
                            if(`${response}` !== 'error'){
                                alert('pago registrado con exito!');
                                $("#paymentModal").modal("hide");
                                location.reload();
                            }
                            
                        },error: function (err) {
                            console.error(err);
                        }
                    }); 
                }    
            }else{
                alert("Error al guardar archivos");
            }                
        },
        error: function(e){
            console.log(e);
        }
    });
});

$("#register_month_payment").submit(function(event) {
    event.preventDefault();
    var unites_quantity = [];
    var form = new FormData();
    form.append("file",$("#ticket")[0].files[0]);
    form.append("unit", unit);
    form.append("route_name", route_name);
    form.append("payment_number", payment_number);

    paymentArray.forEach(element => {
        if($('#cb_' + element).prop('checked') ){
            unites_quantity.push(element);
         }
    });

    $.ajax({
        url: `${base_url}controllers/upload.php`,
        type: "POST",
        data: form,
        processData: false,
        contentType: false,
        success: function(file){
            if(!`${file}`.includes("error")){
                const montoTotal =  $('#amount').val();
                const mont = montoTotal / unites_quantity.length;
                $.ajax({
                    url: `${base_url}controllers/payment/payment_controller.php`,
                    cache: false,
                    type: "POST",
                    dataType: "JSON",
                    data: { action: 5, route_units: JSON.stringify(unites_quantity), monto: mont, file: `${file}`, comment: $('#comments').val(), month: $("#month").val()},
                    success: function(response){
                        console.log(response);
                        if(`${response}` === '1'){
                            alert('pago mensual registrado con exito!');
                            $("#paymentModalMonth").modal("hide");
                            location.reload();
                        }else if(`${response}` === '0'){
                            alert(`Error,Ya hay unidades con registros de ese mes de en el sitema`);
                        }else{
                            alert("A ocurrido un error intente de nuevo mas tarde");
                        }
                        
                    },error: function (err) {
                        console.error(err);
                    }
                }); 
            }else{
                alert("Error al guardar archivos");
            }                
        },
        error: function(e){
            console.log(e);
        }
    });
});

function viewDetails(month) {
    $.ajax({
        url: `${base_url}controllers/payment/payment_controller.php`,
        cache: false,
        type: 'POST',
        dataType: 'JSON',
        data: { action: 8, route_name: route_name, month: month},
        success: function(response){
            $('#ticket_img').html(`<img class="w-100" src="${base_url}storage/${route_name}/M/${response[0].file}" alt="">`);
            var rows = '';
            var montoTotal = 0;
            $.each(response, function(i, unit) {
                rows += i % 5 == 0 ? '<div class="row">' : '';
                rows += `<div class="col">
                            <span>${unit.unit} </span>
                        </div>`;
                rows += i % 5 == 4 ? '</div>' : '';
                montoTotal += +`${unit.monto}`;
            });
            let label = response[0].status==1 ? 'válido' : response[0].status==2 ? 'incorrecto' : response[0].status==3 ? 'info. req.' : 'pendiente';
            let color = response[0].status==1 ? 'success' : response[0].status==2 ? 'danger' : response[0].status==3 ? 'warning' : 'info';
            $("#bStatus").html(label);
            $("#bStatus").removeClass();
            $("#bStatus").addClass(`badge badge-pill badge-${color}`);
            $('#units_rows').html(rows);
            $('#total_amount').html(formatter.format(+`${montoTotal}`));
            $('#total_by_unit').html(formatter.format(+`${response[0].monto}`));
            $('#comments_details').val(response[0].comment);
        },
        error: function(error){
            console.error(error);
        }
    });
    $("#modalTitle").html(`Mensulidad - ${route_name}`);
    $("#detailsMonthModal").modal("show");
}

function checkFile() {
    let file = $("#ticket")[0].files[0];
    if (file.type != "image/jpeg" && file.type != "image/png") {
        $("#ticket").val(null);
        alert('Archivo no soportado');
    }
}

$('#paymentModal').on('hidden.bs.modal', function (e) {
    $('#amount').val(null);
    $('#comments').val(null);
    $('#units_card_rows').html(null);
    $('#ticket').val(null);
    $('#units_card').addClass('d-none');
    $('#observations_card').addClass('d-none');
    $("#observations").html(null);
});

$('#paymentModalMonth').on('hidden.bs.modal', function (e) {
    $('#amount').val(null);
    $('#comments').val(null);
    $('#units_card_rows').html(null);
    $('#ticket').val(null);
    $('#observations_card').addClass('d-none');
    $("#observations").html(null);
    getMonthPayments();
});

$('#detailsMonthModal').on('hidden.bs.modal', function (e) {
    getMonthPayments();
});

function getCountFile(payment_number){
    $.ajax({
        url: `${base_url}controllers/admin/admin_controller.php`,
        cache: false,
        type: 'POST',
        dataType: 'JSON',
        data: { action: 3, payment_number: payment_number, route_name: route_name},
        success: function(response){
            $.each(response, function(index, val) {
                if(val.status==1){
                    $("#buttonP"+payment_number+val.route_payment_id).html("Valido");
                    $('#buttonP'+payment_number+val.route_payment_id).removeClass();
                    $('#buttonP'+payment_number+val.route_payment_id).addClass("btn btn-success w-100");
                }else if(val.status==2){
                    $("#buttonP"+payment_number+val.route_payment_id).html("Incorrecto");
                    $('#buttonP'+payment_number+val.route_payment_id).removeClass();
                    $('#buttonP'+payment_number+val.route_payment_id).addClass("btn btn-danger w-100");
                }else if(val.status==3){
                    $("#buttonP"+payment_number+val.route_payment_id).html("req. inf");
                    $('#buttonP'+payment_number+val.route_payment_id).removeClass();
                    $('#buttonP'+payment_number+val.route_payment_id).addClass("btn btn-warning w-100");
                }else{
                    $("#buttonP"+payment_number+val.route_payment_id).html("revisión");
                    $('#buttonP'+payment_number+val.route_payment_id).removeClass();
                    $('#buttonP'+payment_number+val.route_payment_id).addClass("btn btn-info w-100");
                }
            });
        },
        error: function(error){
            console.error(error);
        }
    });
}

function loadRoutes(){
    $.ajax({
        url: `${base_url}controllers/payment/payment_controller.php`,
        cache: false,
        type: 'POST',
        dataType: 'JSON',
        data: { action: 4},
        success: function (result) {
          let content = "";
            $.each(result, function(index, val){
              content += `<tr scope="row" >
                            <td></td>
                            <td class="w-75">${val.route}</td>
                            <td class="w-25">
                                <button class="btn btn-info btn-sm w-100" onclick="routeData('${val.route}')">Acceder</button>
                            </td>
                          </tr>`;
            });
            $("#content-table-route").append(content);
        },
        error: function(result){
            console.log(result);
        }
    });
}

function deleteMonthPayment(){
    alert('estamos trabajando en esto');
}

function routeData(route){
  console.log(route);
  window.location.href = `${base_url}routes.php?ruta=${route}`;

}

function MonthPaymentUrl(){
    window.location.href = `${base_url}month.php?ruta=${route_name}`;
  }
  