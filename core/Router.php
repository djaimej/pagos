<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$path = array_filter(explode("/", $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']));
$path = buildUrl($path, $config["struct_url"]);


function validateController($url) {
    if (file_exists($url)) {
        return true;
    } else {
        return false;
    }
}

//var_dump($path);

function buildDirController($path) {
    return "./views/" . $path["module"] . "/" . $path["page"] . ".php";
}

function buildUrl($path, $struct) {
    $sizeStruct = count($struct);
    $arr = array();
    for ($i = 0, $p = 0; $i < $sizeStruct && !empty($path[$p]); $i++, $p++) {

        switch ($struct[$i]) {
            case "domain":
                if($GLOBALS["config"]["contains_sessions"]){
                    $arr[$struct[$i]] = $path[$p]."/".$path[$p+1]; 
                $p++;
                }else{
                $arr[$struct[$i]] = $path[$p]; 
                }
                break;
            default :
                $arr[$struct[$i]] = $path[$p];
                break;
                
        }
    }
    return $arr;
}

function base_url() {
    $server_name = $_SERVER['SERVER_NAME'];
    $server_port = $_SERVER['SERVER_PORT'];
    $url = "http://".$server_name.":".$server_port."/pagos/";

    return $url;
}

function all_url($limit = "home") {
    $bool = true;
    $url = "http://";    
    $i = 0;
    $size = count($GLOBALS["path"]);
    while ($bool) {
        $url .= $GLOBALS["path"][$GLOBALS["config"]["struct_url"][$i]]."/";
        if($GLOBALS["config"]["struct_url"][$i]== $limit || $i == $size )
            {
            $bool= false;
        }else{
        $i++;
        }
    }
    return $url;
}

function getURL(){
    $i=0;
    $url ="http://";
    while (isset($GLOBALS["path"][$GLOBALS["config"]["struct_url"][$i]])) {
        $url .= $GLOBALS["path"][$GLOBALS["config"]["struct_url"][$i]]."/";
        $i++;
    }
    return $url;
}