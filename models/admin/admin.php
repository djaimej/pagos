<?php
date_default_timezone_set("America/Mexico_City");

if(file_exists('../models/db_connection.php')){
    require_once '../models/db_connection.php';
}else if(file_exists('../../models/db_connection.php')){
    require_once  '../../models/db_connection.php';
}else if(file_exists('../../../models/db_connection.php')){
    require_once  '../../../models/db_connection.php';
}else if(file_exists('../../db_connection.php')){
    require_once  '../../db_connection.php';
}

class queriesAdmin{

    function getRoutePaymentsAdmin($route_name){
        $con=new DBconnection();
        $con->openDB();

        $res = $con->query("SELECT * FROM route_payment WHERE route_name='".$route_name."' ORDER BY route_payment_id;");
        $data = array();
            while($row = pg_fetch_array($res)){
                $dat = array(
                    "route_payment_id"=>$row["route_payment_id"],
                    "last_name"=>$row["last_name"],
                    "unit"=>$row["unit"],
                    "status"=>$row["status"],
                    "notes"=>$row["notes"],
                    "installation_date"=>$row["installation_date"]    
                );
                $data[] = $dat;
            }
        $con->closeDB();
        return $data;
    }

    function getFile($route_payment_id,$payment_number){
        $con=new DBconnection();
        $con->openDB();

        $res = $con->query("SELECT payment_id, monto, date, file, observations, status, comment FROM payment WHERE route_payment_id=".$route_payment_id." AND payment_number=".$payment_number.";");
        $data = array();
            while($row = pg_fetch_array($res)){
                $dat = array(
                    "payment_id"=>$row["payment_id"],
                    "monto"=>$row["monto"],
                    "date"=>$row["date"],
                    "file"=>$row["file"],
                    "observations"=>$row["observations"],
                    "status"=>$row["status"],
                    "comment"=>$row["comment"]
                );
                $data[] = $dat;
            }
        $con->closeDB();
        return $data;
    }

    function getCountFile($payment_number, $route_name){
        $con=new DBconnection();
        $con->openDB();

        $res = $con->query("SELECT rp.route_payment_id, count(p.payment_number) as payments, p.status FROM route_payment AS rp
                            JOIN payment AS p ON rp.route_payment_id = p.route_payment_id AND p.type = 1
                            WHERE rp.route_name = '".$route_name."' AND p.payment_number = ".$payment_number." GROUP BY rp.route_payment_id, p.status");
        $data = array();
            while($row = pg_fetch_array($res)){
                $dat = array(
                    "route_payment_id"=>$row["route_payment_id"],
                    "payments"=>$row["payments"],
                    "status"=>$row["status"]
                );
                $data[] = $dat;
            }
        $con->closeDB();
        return $data;
    }

    function setStatus($payment_id, $status) {
        $con= new DBconnection();
        $con->openDB();
        $res = $con-> query("UPDATE payment SET status='".$status."' WHERE payment_id=".$payment_id);
        if($res){
            return 1;
        }else {
            return 0;
        }
        $con->closeDB();
    }
    
    function updateObservations($route_payment_id,$payment_number,$observations){
        $con=new DBconnection();
        $con->openDB();

        $query = "";
        if($observations != null) {
            $query = "UPDATE payment SET observations = '".$observations."' 
            WHERE route_payment_id = ".$route_payment_id." AND payment_number =".$payment_number;
        } else {
            $query = "UPDATE payment SET observations = null 
            WHERE route_payment_id = ".$route_payment_id." AND payment_number =".$payment_number;
        }

        $res = $con->query($query);
        if($res) {
            return 1;
        } else {
            return 0;
        }
        $con->closeDB();
    }
    
    function updateMonthObservations($payment_ids,$month,$observations){
        $con=new DBconnection();
        $con->openDB();
        $units = json_decode($payment_ids);

        foreach ($units as $route_payment_id) {
            $res = $con->query("UPDATE payment SET observations = '".$observations."' 
            WHERE payment_id = ".$route_payment_id." AND month ='".$month."'");
        }
        $con->closeDB();
        return 1;
    }

    function getTotalPayment($route_name){
        $con=new DBconnection();
        $con->openDB();

        $data = array();
        for ($i=1; $i < 5; $i++) { 
            $res = $con->query("SELECT SUM(pa.monto) AS total FROM route_payment rp, payment pa 
            WHERE rp.route_payment_id=pa.route_payment_id AND route_name='".$route_name."' AND payment_number=".$i.";");
            $dat = array(
                "total"=>pg_fetch_result($res,0,0)
            );
            $data[] = $dat;
        }
        $con->closeDB();
        return $data;
    }
    
    function UpdateUnitStatus($route_payment_id){
        $con=new DBconnection();
        $con->openDB();

        $res = $con->query("UPDATE route_payment set status = NOT status WHERE route_payment_id =".$route_payment_id);
        if ($res) {
            return 1;
        } else {
            return 0;
        }
        
        $con->closeDB();
    }
    
    function UpdateUnitNotes($route_payment_id,$notes){
        $con=new DBconnection();
        $con->openDB();

        $res = $con->query("UPDATE route_payment set notes = '".$notes."' WHERE route_payment_id =".$route_payment_id);
        if ($res) {
            return 1;
        } else {
            return 0;
        }
        
        $con->closeDB();
    }
    
    function updateInstDate($route_payment_id,$date_inst){
        $con=new DBconnection();
        $con->openDB();
        $res = $con->query("UPDATE route_payment set installation_date = ".$date_inst." WHERE route_payment_id =".$route_payment_id);
        if ($res) {
            return 1;
        } else {
            return 0;
        }
        
        $con->closeDB();
    }

    function getRoutes(){
        $con=new DBconnection();
        $con->openDB();

        $res = $con->query("SELECT route_name, COUNT(route_payment_id) AS units FROM route_payment GROUP BY route_name ORDER BY  route_name");
        $data = array();
            while($row = pg_fetch_array($res)){

                $payments = $con->query("(SELECT COUNT(rp.route_payment_id) FROM route_payment AS rp
                                            JOIN payment AS p ON rp.route_payment_id = p.route_payment_id AND p.payment_number = 1
                                            WHERE rp.route_name ='".$row["route_name"]."')
                                            UNION ALL
                                            (SELECT COUNT(rp.route_payment_id) FROM route_payment AS rp
                                            JOIN payment AS p ON rp.route_payment_id = p.route_payment_id AND p.payment_number = 2
                                            WHERE rp.route_name ='".$row["route_name"]."')
                                            UNION ALL
                                            (SELECT COUNT(rp.route_payment_id) FROM route_payment AS rp
                                            JOIN payment AS p ON rp.route_payment_id = p.route_payment_id AND p.payment_number = 3
                                            WHERE rp.route_name ='".$row["route_name"]."')
                                            UNION ALL
                                            (SELECT COUNT(rp.route_payment_id) FROM route_payment AS rp
                                            JOIN payment AS p ON rp.route_payment_id = p.route_payment_id AND p.payment_number = 4
                                            WHERE rp.route_name ='".$row["route_name"]."')
                                            UNION ALL
                                            (SELECT COUNT(rp.route_payment_id) FROM route_payment AS rp
                                            JOIN payment AS p ON rp.route_payment_id = p.route_payment_id AND p.payment_number = 1 AND p.status = '1'
                                            WHERE rp.route_name ='".$row["route_name"]."')
                                            UNION ALL
                                            (SELECT COUNT(rp.route_payment_id) FROM route_payment AS rp
                                            JOIN payment AS p ON rp.route_payment_id = p.route_payment_id AND p.payment_number = 2 AND p.status = '1'
                                            WHERE rp.route_name ='".$row["route_name"]."')
                                            UNION ALL
                                            (SELECT COUNT(rp.route_payment_id) FROM route_payment AS rp
                                            JOIN payment AS p ON rp.route_payment_id = p.route_payment_id AND p.payment_number = 3 AND p.status = '1'
                                            WHERE rp.route_name ='".$row["route_name"]."')
                                            UNION ALL
                                            (SELECT COUNT(rp.route_payment_id) FROM route_payment AS rp
                                            JOIN payment AS p ON rp.route_payment_id = p.route_payment_id AND p.payment_number = 4 AND p.status = '1'
                                            WHERE rp.route_name ='".$row["route_name"]."')");

                $dat = array(
                    "route"=>$row["route_name"],
                    "units"=>$row["units"],
                    "ant"=>[pg_fetch_result($payments,0,0),pg_fetch_result($payments,4,0)],
                    "p1"=>[pg_fetch_result($payments,1,0),pg_fetch_result($payments,5,0)],
                    "p2"=>[pg_fetch_result($payments,2,0),pg_fetch_result($payments,6,0)],
                    "mens"=>[pg_fetch_result($payments,3,0),pg_fetch_result($payments,7,0)]
                );
                $data[] = $dat;
            }
        $con->closeDB();
        return $data;
    }

    function deleteFile($payment_id){
        $con=new DBconnection();
        $con->openDB();
       
        $res = $con->query("DELETE FROM payment WHERE payment_id =".$payment_id);

        if ($res) {
            return 1;
        } else {
            return 0;
        }
        
        $con->closeDB();
    }
}
?>