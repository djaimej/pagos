<?php
    class DBconnection{
        private $host ="host=localhost ";
        private $port ="port=5432 ";
        private $user ="user=postgres ";
        private $psw ="password=root ";
        private $db ="dbname=payments ";
        
        public function openDB(){
            pg_connect($this->host . $this->port . $this->db. $this->user . $this->psw )or die ("error ".pg_last_error());
        }
        
        public function closeDB(){
            pg_close();
        }
        
        public function query($sql){
            return pg_query($sql);
        }
    }
?>