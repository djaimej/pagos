<?php
date_default_timezone_set("America/Mexico_City");

if(file_exists('../models/db_connection.php')){
}else if(file_exists('../../models/db_connection.php')){
    require_once  '../../models/db_connection.php';
}else if(file_exists('../../../models/db_connection.php')){
    require_once  '../../../models/db_connection.php';
}else if(file_exists('../../db_connection.php')){
    require_once  '../../db_connection.php';
}

class queriesPayment{
	
    function uploadFile($file_name){
        $con=new DBconnection();
        $con->openDB();

        $upload=$con->query("INSERT INTO uploads(file_name) VALUES ('".$file_name."') RETURNING id_upload;");

        $id = pg_fetch_result($upload,0,0);
        if($upload){
            return $id; 
        }
        else{
            return "error";
        }
    
        $con->closeDB();
    }

    function addPayment($route_payment_id,$monto,$payment_number,$file,$comment){
        $con=new DBconnection();
        $con->openDB();
        $current_date = date('Y-m-d H:i:s');

        $query = ($comment != '') ? "INSERT INTO payment(route_payment_id, monto, type, date, payment_number,file,comment) 
        VALUES(".$route_payment_id.", ".$monto.", 1, '".$current_date."', ".$payment_number.", '".$file."','".$comment."') RETURNING payment_id;"
        : "INSERT INTO payment(route_payment_id, monto, type, date, payment_number,file) 
        VALUES(".$route_payment_id.", ".$monto.", 1, '".$current_date."', ".$payment_number.", '".$file."') RETURNING payment_id;";

        $payment=$con->query($query);

        $id = pg_fetch_result($payment,0,0);
        if($payment){
            return $id; 
        }
        else{
            return "error";
        }
    
        $con->closeDB();
    }
    
    function addMonthlyPayment($route_units,$monto,$file,$comment,$month){
        $con=new DBconnection();
        $con->openDB();
        $current_date = date('Y-m-d H:i:s');
        $units = json_decode($route_units);
        $month = date('Y-').$month.'-15';
        $exits = false;
        foreach ($units as $value) {
            $payment=$con->query("SELECT * FROM payment WHERE month = '".$month."' AND route_payment_id = ".$value);
            if(!empty(pg_fetch_array($payment))){
                $exits = true;
            }
        }
        if(!$exits){
            foreach ($units as $value) {
                $payment=$con->query("INSERT INTO payment(route_payment_id, monto, type, date, payment_number, file, comment, month) 
                VALUES(".$value.", ".$monto.", 2, '".$current_date."', 4, '".$file."', '".$comment."', '".$month."') RETURNING payment_id;");
            }
        }else{
            return 0;
        }
        $con->closeDB();
        return 1;
    }

    function getRoutePayments($route_name){
        $con=new DBconnection();
        $con->openDB();

        $res = $con->query("SELECT rp.route_payment_id, rp.last_name, rp.unit, rp.status, rp.notes, rp.installation_date,
            (SELECT type FROM payment WHERE rp.route_payment_id = route_payment_id LIMIT 1) AS type 
            FROM route_payment as rp WHERE rp.route_name='".$route_name."' ORDER BY rp.route_payment_id;");
        $data = array();
            while($row = pg_fetch_array($res)){
                $dat = array(
                    "route_payment_id"=>$row["route_payment_id"],
                    "last_name"=>$row["last_name"],
                    "type"=>$row["type"],
                    "unit"=>$row["unit"],
                    "status"=>$row["status"],
                    "notes"=>$row["notes"],
                    "installation_date"=>$row["installation_date"]
                );
                $data[] = $dat;
            }
        $con->closeDB();
        return $data;
    }

    function getPaymentData($route_payment_id,$payment_number){
        $con=new DBconnection();
        $con->openDB();

        $res = $con->query("SELECT observations, type, status FROM payment WHERE route_payment_id='".$route_payment_id."' AND payment_number=".$payment_number);
        $data = array();
            while($row = pg_fetch_array($res)){
                $dat = array(
                    "observations"=>$row["observations"],
                    "type"=>$row["type"],
                    "status"=>$row["status"]
                );
                $data[] = $dat;
            }
        $con->closeDB();
        return $data;
    }

    function getRoutes(){
        $con=new DBconnection();
        $con->openDB();

        $res = $con->query("SELECT route_name FROM route_payment GROUP BY route_name ORDER BY  route_name;");
        $data = array();
            while($row = pg_fetch_array($res)){
                $dat = array(
                    "route"=>$row["route_name"]
                );
                $data[] = $dat;
            }
        $con->closeDB();
        return $data;
    }

    function addPaymentMultiple($route_payment_id,$monto,$payment_number,$file,$comment){
        $con=new DBconnection();
        $con->openDB();
        $units = json_decode($route_payment_id);
        $current_date = date('Y-m-d H:i:s');

        foreach ($units as $value) {
            $payment=$con->query("INSERT INTO payment(route_payment_id, monto, type, date, payment_number, file,comment) 
            VALUES(".$value.", ".$monto.", 1, '".$current_date."', ".$payment_number.", '".$file."', '".$comment."') RETURNING payment_id;");
        }
        $con->closeDB();
        return 1;
    }

    function getMonthPayments($route_name){
        $con=new DBconnection();
        $con->openDB();

        $res=$con->query("SELECT p.date, p.observations, p.status, p.month, p.comment FROM route_payment AS rp
        JOIN payment AS p ON rp.route_payment_id = p.route_payment_id AND p.type = 2
        WHERE rp.route_name = '".$route_name."' GROUP BY p.month, p.date, p.observations, p.status,  p.comment");
        $data = array();
        while($row = pg_fetch_array($res)){
            $dat = array(
                "date"=>$row["date"],
                "observations"=>$row["observations"],
                "status"=>$row["status"],
                "month"=>$row["month"],
                "comment"=>$row["comment"]
            );
            $data[] = $dat;
        }
        $con->closeDB();
        return $data;
    }
    
    function viewDetails($route_name,$month){
        $con=new DBconnection();
        $con->openDB();

        $res=$con->query("SELECT p.file, p.status, p.payment_id, p.comment, rp.unit, p.monto, p.observations FROM route_payment AS rp
        JOIN payment AS p ON rp.route_payment_id = p.route_payment_id AND p.type = 2
        WHERE rp.route_name = '".$route_name."' AND p.month = '".$month."'");
        $data = array();
        while($row = pg_fetch_array($res)){
            $dat = array(
                "monto"=>$row["monto"],
                "file"=>$row["file"],
                "comment"=>$row["comment"],
                "unit"=>$row["unit"],
                "observations"=>$row["observations"],
                "payment_id"=>$row["payment_id"],
                "status"=>$row["status"]
            );
            $data[] = $dat;
        }
        $con->closeDB();
        return $data;
    }

}
?>