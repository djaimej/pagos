<?php 
include_once 'core/Config.php';
include_once 'core/Router.php';
include_once 'core/Controller.php';
?>
<!DOCTYPE html>
<html lang="es-MX">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-language" content="es">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url()?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url()?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url()?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url()?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url()?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url()?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url()?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url()?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url()?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url()?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url()?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url()?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url()?>assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url()?>assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title>Registro de Mensualidades</title>

    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css">

    <script>
      const base_url = "<?php echo base_url() ?>";
      const route_name = "<?php echo $_GET['ruta'] ?>";
    </script>
</head>

    <body>
        <div class="container d-flex justify-content-between mt-5">
            <h4 id="route_title"></h4>
            <button onclick="addMonthlyPayment()" class="btn btn-secondary btn-sm pl-5 pr-5 w-25" >Nuevo registro</button>
        </div>
        <div class="container justify-content-center mt-2">
            <table id="table_route" class="table table-borderless table-responsive table-sm table-striped table-condensed">
                <thead class="thead-dark">
                    <tr>
                        <th></th>
                        <th class="w-10">Registro</th>
                        <th class="w-20">fecha de registro</th>
                        <th class="w-10">Mes</th>
                        <th class="w-40">Observaciones</th>
                        <th class="w-10">Estado</th>
                        <th class="w-10">Detalles</th>
                    </tr>
                </thead>
                <tbody id="tbody_route" ></tbody>
            </table>
        </div>

        <div class="modal fade" id="paymentModalMonth" tabindex="-1" role="dialog" aria-labelledby="paymentModalMonthLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title w-100 d-flex justify-content-between" id="paymentModalMonthLabel"></div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="register_month_payment">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="ticket" class="col-form-label"><span class="text-danger">*</span>Ticket:</label>
                            <div class="custom-file">
                                <input type="file" class="form-control-file" id="ticket" name="file[]" onchange="checkFile()" accept="image/jpeg, image/png" required lang="es">
                            </div>
                        </div>
                        <div class="form-group"  id="amount_form">
                            <label for="amount" class="col-form-label"><span class="text-danger">*</span>Monto del pago:</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">$</div>
                                </div>
                                <input type="number" onkeyup="amountChenged('amount')" min="0" step="0.01" class="form-control" id="amount" required>
                            </div>
                        </div>
                        <div class="form-group"  id="comments_form">
                            <label for="comments" class="col-form-label">Comentarios:</label>
                            <div class="input-group mb-2">
                                <input type="text" class="form-control" id="comments">
                            </div>
                        </div>
                        <div class="input-group" id="month_form">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">mes: </span>
                                </div>
                                <select class="form-control" id="month">
                                    <option value="01">Enero</option>
                                    <option value="02">Febrero</option>
                                    <option value="03">Marzo</option>
                                    <option value="04">Abril</option>
                                    <option value="05">Mayo</option>
                                    <option value="06">Junio</option>
                                    <option value="07">Julio</option>
                                    <option value="08">Agosto</option>
                                    <option value="09">Septiembre</option>
                                    <option value="10">Octubre</option>
                                    <option value="11">Noviembre</option>
                                    <option value="12">Diciembre</option>
                                </select>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="current_year"></span>
                                </div>
                        </div>
                        <div id="units_card">
                            <div class="card-body bg-light p-2" id="units_card_rows">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Registrar</button>
                    </div>
                </form>
            </div>
        </div>
        </div>

        <div class="modal fade" id="detailsMonthModal" tabindex="-1" role="dialog" aria-labelledby="detailsMonthLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="modal-title w-100 d-flex justify-content-between" id="detailsModalLabel">
                            <h5 id="modalTitle" ></h5> <span id="bStatus" style="height: 1.5em;" class="badge badge-pill badge-info">pendiente</span>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="details_month_payment">
                        <div class="modal-body">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-form-label">Ticket:</label>
                                            <div id="ticket_img"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-form-label">Total del ticket:</label>
                                            <strong id="total_amount"></strong>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Pago por unidad:</label>
                                            <strong id="total_by_unit"></strong>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Unidades registradas en el pago:</label>
                                            <div class="container bg-light p-2" id="units_rows"></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="comments" class="col-form-label">Comentarios:</label>
                                            <div class="input-group mb-2">
                                                <input type="text" class="form-control" id="comments_details">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-dark" data-dismiss="modal">Salir</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
    <footer>
        <script src="<?php echo base_url() ?>assets/js/jquery-3.3.1.js"></script>
        <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>controllers/payment/script_payment.js"></script>
        <script>
            $(function() {
                getMonthPayments();
            });
        </script>
    </footer>
</html>