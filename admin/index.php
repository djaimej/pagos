<?php session_start(); ?>
<?php 
include_once '../core/Config.php';
include_once '../core/Router.php';
include_once '../core/Controller.php';
?>
<!DOCTYPE html>
<html lang="es-MX">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-language" content="es">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url()?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url()?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url()?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url()?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url()?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url()?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url()?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url()?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url()?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url()?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url()?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url()?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url()?>assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url()?>assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title>Pagos Rutas</title>
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css">

    <script>
      var base_url = "<?php echo base_url() ?>";
    </script>
</head>

    <body>
    <?php
        if (isset($_POST["submit"])){
            date_default_timezone_set("America/Mexico_City");
            $access_password = 'adm-'.date('d');
            if($access_password == $_POST['access_password']) { $_SESSION["access"] = 1;?>
                <script src="<?php echo base_url() ?>assets/js/jquery-3.3.1.js"></script>
                <script src="<?php echo base_url() ?>controllers/admin/script_admin.js"></script>
                <script>
                    $(function() {
                        routeData();
                    });
                </script>
    <?php   } else { ?>
        <div class="container mt-5 pt-5 d-flex justify-content-center mt-5">
            <form class="w-50 mt-5" name="form" action="index.php" method="POST">
                <h4 class="text-center">ADMINISTRADOR DE PAGOS</h4>
                <div class="form-group">
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text">Contraseña</div>
                        </div>
                        <input type="password" class="form-control" name="access_password" id="access_password" required>
                    </div>
                </div>
                <div class="form-group d-flex justify-content-center">
                    <button type="submit" name="submit" class="btn btn-primary w-25">Acceder</button>
                </div>
            </form>
        </div>
        
        <div style="margin: 0 auto" class="alert alert-danger alert-dismissible fade show w-50" role="alert">
            <strong>Accesso incorrecto</strong> Intente otra vez.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php   }
        } else {?>
        <?php if(isset($_SESSION["access"])) {
			?>
            <script src="<?php echo base_url() ?>assets/js/jquery-3.3.1.js"></script>
            <script src="<?php echo base_url() ?>controllers/admin/script_admin.js"></script>
            <script>
                $(function() {
                    routeData();
                });
            </script>
        <?php }else{
            ?>
            <div class="container mt-5 pt-5 d-flex justify-content-center mt-5">
                <form class="w-50 mt-5" name="form" action="index.php" method="POST">
                    <h4 class="text-center">ADMINISTRADOR DE PAGOS</h4>
                    <div class="form-group">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Contraseña</div>
                            </div>
                            <input type="password" class="form-control" name="access_password" id="access_password" required>
                        </div>
                    </div>
                    <div class="form-group d-flex justify-content-center">
                        <button type="submit" name="submit" class="btn btn-primary w-25">Acceder</button>
                    </div>
                </form>
            </div>
        <?php } 
        }?>   
        <script src="<?php echo base_url() ?>assets/js/jquery-3.3.1.js"></script>
        <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    </body>
</html>