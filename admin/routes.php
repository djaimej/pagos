<?php session_start(); ?>
<?php 
include_once '../core/Config.php';
include_once '../core/Router.php';
include_once '../core/Controller.php';
?>
<!DOCTYPE html>
<html lang="es-MX">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-language" content="es">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url()?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url()?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url()?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url()?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url()?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url()?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url()?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url()?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url()?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url()?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url()?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url()?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url()?>assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url()?>assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title >Admin</title>

    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css">

    <script>
      const base_url = "<?php echo base_url() ?>";
    </script>
</head>

    <body>
    <?php if(isset($_SESSION["access"])) {
			?>
        <?php if(isset($_GET["ruta"])) {
        ?>
            <script>
                const route_name = "<?php echo $_GET['ruta'] ?>";
            </script>
            <div class="container d-flex justify-content-between mt-5">
                <h4 id="route_title"></h4>
                <button onclick="MonthPaymentUrl()" class="btn btn-secondary btn-sm pl-5 pr-5 w-25" >Pagos mensuales</button>
            </div>
            <div class="justify-content-center mt-2 mx-3">
                <table id="table_route" class="table w-100 table-borderless table-responsive table-sm table-striped table-condensed">
                    <thead class="thead-dark">
                        <tr>
                            <th class="w-5">Unidad</th>
                            <th class="w-20">Nombre</th>
                            <th class="w-10">Anticipo </th>
                            <th class="w-10">Pago 1</th>
                            <th class="w-10">Pago 2</th>
                            <th class="w-10">Estado</th>
                            <th class="w-10">Instalación</th>
                            <th class="w-25">Notas</th>
                        </tr>
                    </thead>
                    <tbody id="tbody_route" ></tbody>
                </table>
            </div>

        <div class="modal fade" id="filesModal" tabindex="-1" role="dialog" aria-labelledby="filesModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="modal-title w-100 d-flex justify-content-between" id="filesModalLabel">
                            <h5 id="modalTitle" ></h5> <span id="bStatus" style="height: 1.5em;" class="badge badge-pill badge-info">pendiente</span>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="register_payment">
                        <div class="modal-body">
                            <table id="files" class="table w-100 table-borderless table-responsive table-sm table-striped table-condensed">
                                <thead class="thead-dark">
                                    <tr>
                                        <th></th>
                                        <th class="w-20">Monto</th>
                                        <th class="w-20">Fecha</th>
                                        <th class="w-20">Ticket</th>
                                        <th class="w-35">Comentarios</th>
                                        <th class="w-5"></th>
                                    </tr>
                                </thead>
                                <tbody id="tbody-file"></tbody>
                            </table>
                            <div class="form-group mb-0">
                                <label for="observations" class="col-form-label">Observaciones:</label>
                                <textarea class="form-control" id="observations"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" onclick="setStatus(1)" class="btn btn-success">Valido</button>
                            <button type="button" onclick="setStatus(2)" class="btn btn-danger" >Incorrecto</button>
                            <button type="button" onclick="setStatus(3)" class="btn btn-warning">Necesita más Información</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" id="modalDelete" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Confirmación de eliminación</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="form-control" id="payment_id">
                    <p>¿Esta seguro de eliminar este registro?.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-danger" onclick="deleteFile()">Confirmar</button>
                </div>
                </div>
            </div>
        </div>

        <script src="<?php echo base_url() ?>assets/js/jquery-3.3.1.js"></script>
        <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>controllers/admin/script_admin.js"></script>
        <script>
            $(async function() {
                await getRoutePaymentsAdmin();
                for(var i=1;i<4;i++){
                    getCountFile(i);
                }
            });
        </script>
            <?php } else {
			?>
                <div class="mr-5 ml-5 justify-content-center mt-5">
                    <table class="table table-borderless table-responsive table-sm table-striped table-condensed">
                        <thead class="thead-dark">
                            <tr>
                                <th></th>
                                <th class="w-30">Ruta</th>
                                <th class="w-15">Anticipo</th>
                                <th class="w-15">Pago 1</th>
                                <th class="w-15">Pago 2</th>
                                <th class="w-15">Mensualidad</th>
                                <th class="w-10"s>Acción</th>
                            </tr>
                        <thead>
                        <tbody id="content-table-route">
                        </tbody>
                    </table>
                </div>
                <script src="<?php echo base_url() ?>assets/js/jquery-3.3.1.js"></script>
                <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
                <script src="<?php echo base_url() ?>controllers/admin/script_admin.js"></script>
                <script>
                    $(function() {
                        loadRoutesAdm();
                    });
                </script>
        <?php }
        } else {
            ?>
            <div class="container justify-content-center mt-5">
                <h3 style="text-align: center;">No se ha iniciado sesión: <span class="badge badge-light"><a href="<?php echo base_url() ?>admin/index.php">Volver</a></span></h3>
            </div>
    <?php }
            ?>
    </body>
</html>