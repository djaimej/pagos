<?php session_start(); ?>
<?php 
include_once '../core/Config.php';
include_once '../core/Router.php';
include_once '../core/Controller.php';
?>
<!DOCTYPE html>
<html lang="es-MX">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-language" content="es">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url()?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url()?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url()?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url()?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url()?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url()?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url()?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url()?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url()?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url()?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url()?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url()?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url()?>assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url()?>assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title>Admin</title>

    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css">

    <script>
      const base_url = "<?php echo base_url() ?>";
    </script>
</head>

    <body>
    <?php if(isset($_SESSION["access"])) {
			?>
            <?php if(isset($_GET["ruta"])) {
            ?>
            <script>
                const route_name = "<?php echo $_GET['ruta'] ?>";
            </script>
            <div class="container d-flex justify-content-between mt-5">
                <h4 id="route_title"></h4>
            </div>
            <div class="container justify-content-center mt-2">
                <table id="table_route" class="table table-borderless table-responsive table-sm table-striped table-condensed">
                    <thead class="thead-dark">
                        <tr>
                            <th></th>
                            <th class="w-10">Registro</th>
                            <th class="w-20">fecha de registro</th>
                            <th class="w-10">Mes</th>
                            <th class="w-40">Comentarios</th>
                            <th class="w-10">Estado</th>
                            <th class="w-10">Detalles</th>
                        </tr>
                    </thead>
                    <tbody id="tbody_month_route" ></tbody>
                </table>
            </div>
            <div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="detailsModalLabel" aria-hidden="true">
                <div class="modal-dialog  modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="modal-title w-100 d-flex justify-content-between" id="detailsModalLabel">
                                <h5 id="modalTitle" ></h5> <span id="bStatus" style="height: 1.5em;" class="badge badge-pill badge-info">pendiente</span>
                            </div>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="details_month_payment">
                            <div class="modal-body">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Ticket:</label>
                                                <div id="ticket_img"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Total del ticket:</label>
                                                <strong id="total_amount"></strong>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label">Pago por unidad:</label>
                                                <strong id="total_by_unit"></strong>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label">Unidades registradas en el pago:</label>
                                                <div class="container bg-light p-2" id="units_rows"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="observations" class="col-form-label">Observaciones:</label>
                                                <textarea class="form-control" id="observations"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" onclick="deleteMonthPayment()"class="btn btn-danger">Borrar registro</button>
                                <div class="d-flex">
                                    <button type="button" onclick="setStatus(1)" class="btn mr-1 btn-success">Valido</button>
                                    <button type="button" onclick="setStatus(2)" class="btn mx-1 btn-danger" >Incorrecto</button>
                                    <button type="button" onclick="setStatus(3)" class="btn ml-1 btn-warning">Necesita más Información</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <script src="<?php echo base_url() ?>assets/js/jquery-3.3.1.js"></script>
            <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url() ?>controllers/admin/script_admin.js"></script>
            <script>
                $(function() {
                    getMonthPayments();
                });
            </script>
            <?php } else {
			?>
                <div class="mr-5 ml-5 justify-content-center mt-5">
                    <table class="table table-borderless table-responsive table-sm table-striped table-condensed">
                        <thead class="thead-dark">
                            <tr>
                                <th></th>
                                <th class="w-30">Ruta</th>
                                <th class="w-15">Anticipo</th>
                                <th class="w-15">Pago 1</th>
                                <th class="w-15">Pago 2</th>
                                <th class="w-15">Mensualidad</th>
                                <th class="w-10"s>Acción</th>
                            </tr>
                        <thead>
                        <tbody id="content-table-route">
                        </tbody>
                    </table>
                </div>
                <script src="<?php echo base_url() ?>assets/js/jquery-3.3.1.js"></script>
                <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
                <script src="<?php echo base_url() ?>controllers/admin/script_admin.js"></script>
                <script>
                    $(function() {
                        loadRoutesAdm();
                    });
                </script>
        <?php }
        } else {
            ?>
            <div class="container justify-content-center mt-5">
                <h3 style="text-align: center;">No se ha iniciado sesión: <span class="badge badge-light"><a href="<?php echo base_url() ?>admin/index.php">Volver</a></span></h3>
            </div>
    <?php }
            ?>
    </body>
</html>